package abacus

import (
	"testing"
	"time"

	"gotest.tools/v3/assert"
)

func Test_casket_Add(t *testing.T) {
	type args struct {
		val metricRec
	}
	tests := []struct {
		name        string
		args        []args
		wantBuckets []bucket
	}{
		{
			name: "Should create buckets by second",
			args: []args{
				{val: metricRec{Value: 1, CreatedAt: time.Date(2023, 24, 1, 0, 0, 0, 300, time.UTC)}},
				{val: metricRec{Value: 2, CreatedAt: time.Date(2023, 24, 1, 0, 0, 0, 500, time.UTC)}},
				{val: metricRec{Value: 3, CreatedAt: time.Date(2023, 24, 1, 0, 0, 0, 900, time.UTC)}},
				{val: metricRec{Value: 4, CreatedAt: time.Date(2023, 24, 1, 0, 0, 1, 120, time.UTC)}},
			},
			wantBuckets: []bucket{
				{
					Key: time.Date(2023, 24, 1, 0, 0, 0, 0, time.UTC), Recs: []metricRec{
						{Value: 1, CreatedAt: time.Date(2023, 24, 1, 0, 0, 0, 300, time.UTC)},
						{Value: 2, CreatedAt: time.Date(2023, 24, 1, 0, 0, 0, 500, time.UTC)},
						{Value: 3, CreatedAt: time.Date(2023, 24, 1, 0, 0, 0, 900, time.UTC)},
					},
				},
				{
					Key: time.Date(2023, 24, 1, 0, 0, 1, 0, time.UTC), Recs: []metricRec{
						{Value: 4, CreatedAt: time.Date(2023, 24, 1, 0, 0, 1, 120, time.UTC)},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(
			tt.name, func(t *testing.T) {
				c := newCasket()
				for _, a := range tt.args {
					c.Add(a.val)
				}

				assert.DeepEqual(t, c.buckets, tt.wantBuckets)
			},
		)
	}
}

func Test_findMinGtIdx(t *testing.T) {
	type args struct {
		recs  []metricRec
		start time.Time
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Should return first index when start is earlier than first element CreatedAt",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 0, 0, 0, 0, time.UTC),
			},
			want: 0,
		},

		{
			name: "Should return first index when start is equals to first element CreatedAt",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC),
			},
			want: 0,
		},

		{
			name: "Should return first index when start is equals to first element CreatedAt and some next elements have same CreatedAt",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC),
			},
			want: 0,
		},

		{
			name: "Should return minimum index when start is earlier than middle element CreatedAt and greater than previous element CreatedAt",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 2, 1, 0, 0, time.UTC),
			},
			want: 2,
		},

		{
			name: "Should return minimum index when start is equals to middle element CreatedAt and greater than previous element CreatedAt",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC),
			},
			want: 2,
		},

		{
			name: "Should return minimum index when start is equals to middle element CreatedAt and greater than previous element CreatedAt and some next elements have same CreatedAt",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC),
			},
			want: 2,
		},

		{
			name: "Should return last index when start is earlier than last element CreatedAt and greater than previous element CreatedAt",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 4, 1, 0, 0, time.UTC),
			},
			want: 4,
		},

		{
			name: "Should return last index when start is equals to last element CreatedAt and greater than previous element CreatedAt",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC),
			},
			want: 4,
		},

		{
			name: "Should return -1 when start is greater than last element CreatedAt",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 6, 0, 0, 0, time.UTC),
			},
			want: -1,
		},
	}
	for _, tt := range tests {
		t.Run(
			tt.name, func(t *testing.T) {
				if got := findMinGtIdx(tt.args.recs, tt.args.start); got != tt.want {
					t.Errorf("findMinGtIdx() = %v, want %v", got, tt.want)
				}
			},
		)
	}
}

func Test_findMinGtIdxBinary(t *testing.T) {
	type args struct {
		recs  []metricRec
		start time.Time
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Should return first index when start is earlier than first element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 0, 0, 0, 0, time.UTC),
			},
			want: 0,
		},

		{
			name: "Should return first index when start is equals to first element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC),
			},
			want: 0,
		},

		{
			name: "Should return first index when start is equals to first element Time and some next elements have same Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC),
			},
			want: 0,
		},

		{
			name: "Should return minimum index when start is earlier than middle element Time and greater than previous element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 2, 1, 0, 0, time.UTC),
			},
			want: 2,
		},

		{
			name: "Should return minimum index when start is equals to middle element Time and greater than previous element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC),
			},
			want: 2,
		},

		{
			name: "Should return minimum index when start is equals to middle element Time and greater than previous element Time and some next elements have same Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC),
			},
			want: 2,
		},

		{
			name: "Should return last index when start is earlier than last element Time and greater than previous element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 4, 1, 0, 0, time.UTC),
			},
			want: 4,
		},

		{
			name: "Should return last index when start is equals to last element Time and greater than previous element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC),
			},
			want: 4,
		},

		{
			name: "Should return -1 when start is greater than last element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				start: time.Date(2023, 1, 24, 6, 0, 0, 0, time.UTC),
			},
			want: -1,
		},
	}
	for _, tt := range tests {
		t.Run(
			tt.name, func(t *testing.T) {
				if got := findMinGtOrEqIdxBinary(tt.args.recs, tt.args.start); got != tt.want {
					t.Errorf("findMinGtIdx() = %v, want %v", got, tt.want)
				}
			},
		)
	}
}

func Test_findMaxLtIndexBinary(t *testing.T) {
	type args struct {
		recs []metricRec
		end  time.Time
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Should return last index when end is after than last element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				end: time.Date(2023, 1, 24, 6, 0, 0, 0, time.UTC),
			},
			want: 4,
		},

		{
			name: "Should return last index when end is equals to last element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				end: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC),
			},
			want: 4,
		},

		{
			name: "Should return last index when end is equals to last element Time and some previous elements have same Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
				},
				end: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC),
			},
			want: 4,
		},

		{
			name: "Should return maximum index when last is after than middle element Time and before than next element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				end: time.Date(2023, 1, 24, 3, 1, 0, 0, time.UTC),
			},
			want: 2,
		},

		{
			name: "Should return maximum index when end is equals to middle element Time and before previous element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				end: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC),
			},
			want: 2,
		},

		{
			name: "Should return maximum index when end is equals to middle element Time and before previous element Time and some previous elements have same Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
				},
				end: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC),
			},
			want: 2,
		},

		{
			name: "Should return first index when end is after than first element Time and before next element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				end: time.Date(2023, 1, 24, 1, 1, 0, 0, time.UTC),
			},
			want: 0,
		},

		{
			name: "Should return first index when end is equals to first element Time and lower than next element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				end: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC),
			},
			want: 0,
		},

		{
			name: "Should return -1 when end is before first element Time",
			args: args{
				recs: []metricRec{
					{CreatedAt: time.Date(2023, 1, 24, 1, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 2, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 3, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 4, 0, 0, 0, time.UTC)},
					{CreatedAt: time.Date(2023, 1, 24, 5, 0, 0, 0, time.UTC)},
				},
				end: time.Date(2023, 1, 24, 0, 0, 0, 0, time.UTC),
			},
			want: -1,
		},
	}
	for _, tt := range tests {
		t.Run(
			tt.name, func(t *testing.T) {
				if got := findMaxLtOrEqIdxBinary(tt.args.recs, tt.args.end); got != tt.want {
					t.Errorf("findMaxLtOrEqIdxBinary() = %v, want %v", got, tt.want)
				}
			},
		)
	}
}

func Benchmark_findMinGtIdx(b *testing.B) {
	const elementsCount = 1000000
	s := make([]metricRec, 0, elementsCount)
	var startDate = time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC)
	for i := 0; i < elementsCount; i++ {
		s = append(s, metricRec{CreatedAt: startDate.Add(time.Duration(i*i) * time.Millisecond)})
	}
	type args struct {
		recs  []metricRec
		start time.Time
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "First element equals",
			args: args{
				recs:  s,
				start: s[0].CreatedAt,
			},
		},

		{
			name: "Last element equals",
			args: args{
				recs:  s,
				start: s[elementsCount-1].CreatedAt,
			},
		},

		{
			name: "Middle element equals",
			args: args{
				recs:  s,
				start: s[elementsCount/2].CreatedAt,
			},
		},
	}
	for _, tt := range tests {
		b.Run(
			tt.name, func(b *testing.B) {
				for i := 0; i < b.N; i++ {
					findMinGtIdx(tt.args.recs, tt.args.start)
				}
			},
		)
	}
}

func Benchmark_findMinGtIdxBinary(b *testing.B) {
	const elementsCount = 1000000
	s := make([]metricRec, 0, elementsCount)
	var startDate = time.Date(2023, 1, 1, 0, 0, 0, 0, time.UTC)
	for i := 0; i < elementsCount; i++ {
		s = append(s, metricRec{CreatedAt: startDate.Add(time.Duration(i*i) * time.Millisecond)})
	}
	type args struct {
		recs  []metricRec
		start time.Time
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "First element equals",
			args: args{
				recs:  s,
				start: s[0].CreatedAt,
			},
		},

		{
			name: "Last element equals",
			args: args{
				recs:  s,
				start: s[elementsCount-1].CreatedAt,
			},
		},

		{
			name: "Middle element equals",
			args: args{
				recs:  s,
				start: s[elementsCount/2].CreatedAt,
			},
		},
	}
	for _, tt := range tests {
		b.Run(
			tt.name, func(b *testing.B) {
				for i := 0; i < b.N; i++ {
					findMinGtRecIdx(tt.args.recs, tt.args.start)
				}
			},
		)
	}
}
