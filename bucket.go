package abacus

import "time"

type bucket struct {
	Key  time.Time
	Recs []metricRec
}

func (b bucket) Time() time.Time {
	return b.Key
}
