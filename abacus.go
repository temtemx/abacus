package abacus

import (
	"context"
	"time"

	"github.com/google/uuid"
	"golang.org/x/exp/constraints"
)

var s = initStore()

func initStore() *bucketStore {
	s := newBucketStore(timeClock{})
	s.startAccountant(context.Background())
	return s
}

type Number interface {
	constraints.Integer | constraints.Float
}

type Counter struct {
	store *bucketStore

	name string
}

func NewCounter() *Counter {
	return &Counter{
		store: s,

		name: uuid.New().String(),
	}
}

func (c *Counter) Inc() {
	c.Add(1)
}

func (c *Counter) Add(value float64) {
	c.store.AddValue(c.name, value)
}

func (c *Counter) Sum(start time.Time, end time.Time) float64 {
	metrics := c.store.Get(c.name, start, end)
	sum := sum(metrics)
	return sum
}

func (c *Counter) Avg(start time.Time, end time.Time) float64 {
	metrics := c.store.Get(c.name, start, end)
	if len(metrics) == 0 {
		return 0
	}
	sum := sum(metrics)
	return sum / float64(len(metrics))
}

func (c *Counter) Rate(start time.Time, end time.Time) float64 {
	metrics := c.store.Get(c.name, start, end)
	if len(metrics) == 0 {
		return 0
	}

	sum := sum(metrics)
	seconds := end.Sub(metrics[0].CreatedAt).Seconds()
	return sum / seconds
}

func sum(vals []metricRec) float64 {
	var sum float64
	for _, v := range vals {
		sum += v.Value
	}
	return sum
}

type Gauge struct {
	store *bucketStore

	name string
}

func NewGauge() *Gauge {
	return &Gauge{
		store: s,

		name: uuid.New().String(),
	}
}

func (g *Gauge) Set(value float64) {
	g.store.AddValue(g.name, value)
}

func (g *Gauge) Value() float64 {
	rec := g.store.Last(g.name)
	return rec.Value
}

func (g *Gauge) Avg(start time.Time, end time.Time) float64 {
	metrics := g.store.Get(g.name, start, end)
	if len(metrics) == 0 {
		return 0
	}
	sum := sum(metrics)
	return sum / float64(len(metrics))
}
