package abacus

import (
	"testing"
	"time"

	"gotest.tools/assert"
)

func TestAvg(t *testing.T) {
	type args struct {
		start time.Time
		end   time.Time
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "Should return 0 if no points in interval",
			args: args{
				start: time.Now().Add(-30 * time.Second),
				end:   time.Now(),
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(
			tt.name, func(t *testing.T) {
				c := NewCounter()
				got := c.Avg(tt.args.start, tt.args.end)
				assert.Equal(t, got, tt.want)
			},
		)
	}
}
