package abacus

import (
	"sync"
	"time"
)

type casket struct {
	mux     *sync.RWMutex
	buckets []bucket
}

func newCasket() *casket {
	return &casket{
		mux:     &sync.RWMutex{},
		buckets: make([]bucket, 0, 1),
	}
}

func (c *casket) Last() metricRec {
	b := c.buckets[len(c.buckets)-1]
	return b.Recs[len(b.Recs)-1]
}

func (c *casket) CutBuckets(start time.Time, end time.Time, interval time.Duration) []bucket {
	start = start.UTC()
	end = end.UTC()
	c.mux.RLock()
	defer c.mux.RUnlock()
	startBucketIdx := findMinGtOrEqBucketIdx(c.buckets, start)
	if startBucketIdx == -1 {
		return nil
	}

	endBucketIdx := findMaxLtOrEqBucketIdx(c.buckets, end)
	if endBucketIdx == -1 {
		return nil
	}

	startMetricIdx := findMinGtOrEqIdxBinary(c.buckets[startBucketIdx].Recs, start)
	endMetricIdx := findMaxLtOrEqIdxBinary(c.buckets[endBucketIdx].Recs, end)
	splittedBuckets := make([]bucket, endBucketIdx-startBucketIdx+1)
	for i, b := range c.buckets[startBucketIdx : endBucketIdx+1] {
		var (
			startIdx = 0
			endIdx   = len(b.Recs) - 1
		)
		if i == 0 {
			if startMetricIdx == -1 {
				continue
			}
			startIdx = startMetricIdx
		}
		if i == endBucketIdx-startBucketIdx && endMetricIdx != -1 {
			if endMetricIdx == -1 {
				continue
			}
			endIdx = endMetricIdx
		}
		splittedBuckets = append(splittedBuckets, bucket{Key: b.Key, Recs: b.Recs[startIdx : endIdx+1]})
	}
	if interval == 0 {
		return splittedBuckets
	}
	return merge(splittedBuckets, start, end, interval)
}

func (s *casket) Add(m metricRec) {
	key := bucketKey(m.CreatedAt)
	s.mux.Lock()
	if len(s.buckets) == 0 || s.buckets[len(s.buckets)-1].Key != key {
		s.buckets = append(s.buckets, bucket{Key: key})
	}
	s.buckets[len(s.buckets)-1].Recs = append(s.buckets[len(s.buckets)-1].Recs, m)
	s.mux.Unlock()
}

func bucketKey(t time.Time) time.Time {
	return t.Add(-time.Duration(t.Nanosecond()))
}

func merge(buckets []bucket, start time.Time, end time.Time, interval time.Duration) []bucket {
	merged := make([]bucket, 0, end.Sub(start)/interval+1)
	cur := 0
	for i := start; i.Before(end); i = i.Add(interval) {
		b := bucket{Key: i}
		for cur < len(buckets) && buckets[cur].Key.Before(i.Add(interval)) {
			if buckets[cur].Key.After(i) {
				b.Recs = append(b.Recs, buckets[cur].Recs...)
			}
		}
		merged = append(merged, b)
	}
	return merged
}

func findMinGtIdx(recs []metricRec, start time.Time) int {
	for i := len(recs) - 1; i > 0; i-- {
		if recs[i].CreatedAt.Before(start) {
			if len(recs) > i+1 {
				return i + 1
			}
			return -1
		}
	}

	if !recs[0].CreatedAt.Before(start) {
		return 0
	}

	return -1
}

func findMinGtRecIdx(recs []metricRec, t time.Time) int {
	return findMinGtOrEqIdxBinary(recs, t)
}

func findMinGtOrEqBucketIdx(buckets []bucket, t time.Time) int {
	key := bucketKey(t)
	return findMinGtOrEqIdxBinary(buckets, key)
}

func findMaxLtOrEqBucketIdx(buckets []bucket, t time.Time) int {
	key := bucketKey(t)
	return findMaxLtOrEqIdxBinary(buckets, key)
}

type Timed interface {
	Time() time.Time
}

func findMinGtOrEqIdxBinary[T Timed](recs []T, t time.Time) int {
	if len(recs) == 0 {
		return -1
	}
	if !recs[0].Time().Before(t) {
		return 0
	}

	var (
		left  = -1
		right = len(recs)
		cur   = right / 2
	)
	for left < cur && cur < right {
		key := recs[cur].Time()
		if key.Before(t) && len(recs) > cur+1 && !recs[cur+1].Time().Before(t) {
			return cur + 1
		}

		if key.Before(t) {
			left = cur
			cur = left + (right-left)/2
			continue
		}

		right = cur
		cur = left + (right-left)/2
	}

	return -1
}

func findMaxLtOrEqIdxBinary[T Timed](recs []T, t time.Time) int {
	if len(recs) == 0 {
		return -1
	}
	lastIdx := len(recs) - 1
	if !recs[lastIdx].Time().After(t) {
		return lastIdx
	}

	var (
		left  = -1
		right = len(recs)
		cur   = right / 2
	)
	for left < cur && cur < right {
		key := recs[cur].Time()
		if key.After(t) && cur-1 >= 0 && !recs[cur-1].Time().After(t) {
			return cur - 1
		}

		if key.After(t) {
			right = cur
			cur = left + (right-left)/2
			continue
		}

		left = cur
		cur = left + (right-left)/2
	}

	return -1
}
