module gitlab.com/temtemx/abacus

go 1.19

require (
	golang.org/x/exp v0.0.0-20220909182711-5c715a9e8561
	gotest.tools v2.2.0+incompatible
)

require (
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	gotest.tools/v3 v3.5.1 // indirect
)
