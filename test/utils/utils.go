package utils

func MustPopFirst[T any](sp *[]T) T {
	s := *sp
	r := s[0]
	*sp = s[1:]
	return r
}
