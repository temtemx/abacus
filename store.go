package abacus

import (
	"context"
	"log"
	"sync"
	"time"
)

type clock interface {
	NowUTC() time.Time
}

type timeClock struct {
}

func (t timeClock) NowUTC() time.Time {
	return time.Now().UTC()
}

type metricRec struct {
	Value     float64
	CreatedAt time.Time
}

func (m metricRec) Time() time.Time {
	return m.CreatedAt
}

type addedMetric struct {
	Name      string
	Value     float64
	CreatedAt time.Time
}

type bucketStore struct {
	clock          clock
	caskets        map[string]*casket
	accountantChan chan addedMetric

	wg *sync.WaitGroup
}

func newBucketStore(clock clock) *bucketStore {
	var wg sync.WaitGroup
	return &bucketStore{clock: clock, caskets: make(map[string]*casket), wg: &wg}
}

func (s *bucketStore) AddValue(name string, val float64) {
	s.accountantChan <- addedMetric{Name: name, Value: val, CreatedAt: s.clock.NowUTC()}
}

func (s *bucketStore) Get(name string, start time.Time, end time.Time) []metricRec {
	c, ok := s.caskets[name]
	if !ok {
		return nil
	}

	buckets := c.CutBuckets(start, end, 0)
	metrics := make([]metricRec, 0)
	for _, b := range buckets {
		metrics = append(metrics, b.Recs...)
	}
	return metrics
}

func (s *bucketStore) Last(name string) metricRec {
	if c, ok := s.caskets[name]; ok {
		return c.Last()
	}
	return metricRec{}
}

func (s *bucketStore) startAccountant(ctx context.Context) {
	var (
		logPref = "[abacus.accountant]"
	)
	s.accountantChan = make(chan addedMetric)
	s.wg.Add(1)
	go func() {
		defer s.wg.Done()
		s.accountantLoop(ctx, logPref, s.accountantChan)
	}()
}

func (s *bucketStore) accountantLoop(ctx context.Context, logPref string, in <-chan addedMetric) {

	for {
		select {
		case <-ctx.Done():
			log.Printf("%s context done", logPref)
			return
		case m, ok := <-in:
			if !ok {
				log.Printf("%s input channel closed", logPref)
			}
			s.add(m)
		}
	}
}

func (s *bucketStore) add(m addedMetric) {
	_, ok := s.caskets[m.Name]
	if !ok {
		s.caskets[m.Name] = newCasket()
	}

	s.caskets[m.Name].Add(metricRec{Value: m.Value, CreatedAt: m.CreatedAt})
}
